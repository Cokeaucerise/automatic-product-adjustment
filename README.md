# Guide d'utilisation du projet

Le document ci-dessous a pour but de montrer comment utiliser le projet et comment changer certaines parties.

# Table de matière
[Executer le projet](#executer-le-projet)

[Environnement de travail](#environnement-de-travail)

[Générer les fichiers](#générer-les-fichiers)

[Analyse graphique de données](#analyse-graphiques-de-données)

[Flux de processus](#flux-de-processus)

# Prérequis

## Utiliser des données personnalisées
NOTE: La section suivante est seulement si vous n'utilisez pas les données par défaut

Afin d'utiliser des données personnalisées pour utiliser le réseau de neurones, vos données doivent contenir certains éléments importants.  Nous vous recommandons d'utiliser un fichier du type `TSV` pour représenter vos données. Si vous ne pouvez pas, assurez-vous de bien changer les paramètres pour l'extraction de labels et de product features. Vous aurez besoin de séparer vos données en 2 fichiers:

1 fichier pour les données de vente
1 fichier pour les détails sur les items

Voici ce qu'il vous faut dans chacun.

### Données de ventes
Chaque entrée dans les données de vente doit représenter la donnée d'une vente d'un item. Si une entrée ou plusieurs entrées peuvent contenir plusieurs items attachés à cette entrée, alors les résultats vont être faussés.

Voici ce que chaque entrée doit avoir:

 - Un identifiant pour l'entrée de ventes  
 - Un identifiant pour la lier à l'item en question
 - La date de vente 
 - Un identifiant ou un symbole pour indiquer sa région
 - La quantité

Le réseau de neurones ne prend pas en compte les ventes négatives ou ceux de 0. Si vous ne les enlevez pas vous-même, le programme ne va simplement pas les considérer.

### Données des items

Chaque item de votre fichier doit au minimum contenir:

 - Un ID
 - Une description
 - Une catégorie
 
 D'autres caractéristiques qui pourraient être intéressantes que vous puissiez ajouter comme colonne:
 
 - La taille
 - Le poid
 - Couleur
 
 Toutes caractéristiques pouvant être représentées de façon atomique pourraient aider à l'apprentissage machine.

 

### Modification du code

Maintenant que vous avez vos données, il y'a quelques endroits où vous devriez modifier le code. Le projet fut programmé pour utiliser des données spécifiques à la base. Il est toutefois possible de faire entraîner le réseau de neurones en fessant quelques modifications.

 - Dans le fichier `create_labels.py` dans la méthode main, changez les colonnes attendues pour les colonnes de votre fichier de vente.
 - Encore dans le fichier `create_labels.py`, il va sûrement falloir que vous changiez le paramètre `-wf` pour filtrer vos données avec le bon symbole qui identifie la région de votre vente (si vous en avez. Si vous n’en avez pas, simplement rien changé)
 -  Dans le fichier `product_features.py`, changer les `strings` dans la variable `ITEM_COLS` pour représenter les colonnes de votre fichier d'items

Avec ces modifications, vos données personnalisées devraient fonctionner.

# Environnement de travail

Si vous possédez une carte NVIDIA, il faut que vous installiez CUDA pour permettre l'utilisation de la carte graphique.

Si vous n'avez pas de carte graphique NVIDIA ou que CUDA n'est pas installé, le CPU va être utilisé

Pour savoir comment installer CUDA sur votre machine et quelle version il faut installer, veuillez consulter cette [page](https://www.tensorflow.org/install/gpu)

Nous vous recommandons aussi d'utiliser [Visual Studio Code](https://code.visualstudio.com/) pour exécuter le projet puisque la majorité des tasks ont été écrite pour fonctionner avec cet IDE.

## Prérequis pour rouler avec Docker

Le projet fonctionne avec `Python 3.7`. Il est donc nécessaire d'avoir cette version ainsi que toutes les librairies dans le fichier `requirements.txt`

### L'ordinateur Hôte est Linux

1. Installer le NVIDIA Container ToolKit : https://github.com/NVIDIA/nvidia-docker
2. Utiliser la commande pour utiliser le GPU avec le docker. Lien du DOC : https://hub.docker.com/r/tensorflow/tensorflow/

### L'ordinateur Hôte est Windows

Malheureusement, Windows ne supporte pas Le NIVIDIA Container Toolkit. Il existe un Toolkit vraiment instable si vous voulez l'utiliser, mais nous recommandons
de ne pas utiliser Docker avec Windows.

# Générer les fichiers

Ce projet vient avec deux données de contexte, soit la température et les fêtes. Généré un d'eux pour les combiner
avec les données de produits. Le projet a aussi besoin d'un fichier de product features et d'un fichier de labels.

NOTE: Les données par défaut ne fonctionnent qu'avec les données de contexte météorologique pour l'instant.

### Générer les données météo

1. Ouvrir un terminal et déplacez-vous dans le dossier data/weather
2. Exécuter le fichier `forecast_weather.py` pour générer les prévisions météorologiques ou `historical_weather_point.py` pour générer l'historique de météo.
Chacun prend des paramètres différents. Veuillez prendre note que chacun génère des données que pour un seul endroit.

Voici un exemple à quoi doit ressembler votre commande:

```
Python historical_weather_point.py -a "20 test stret" -s "2015.01.01" -e "2020.01.01" > forecast_weather.csv 
```

 
 Pour plus d'information sur comment les deux fichiers fonctionnent, veuillez consulter la documentation dans les deux fichiers.

### Générer le fichier de products features

Simplement utiliser la task de création de products features si vous avez `VS Code`. Sinon, simplement exécuter le `fichier product_features.py` avec le fichier d'item en entrée système.

Exemple de commande:
```
Python ./src/product_features.py < [YOUR_FILE_HERE.py] > ./data/output/p_features.csv
```

### Générer le fichier de labels

Avec `VS Code`, simplement exécuter une des tâches commençant avec "Create Labels whXX". Ceci va vous créer un fichier de labels pour un warehouse en particulier.

Les tasks sont faites pour fonctionner avec les données par défaut. Pour créer un fichier de label avec des données personnalisées, vous pouvez changer paramètres pour indiquer le format des dates, le caractère pour indiquer chaque colonne, etc. Veuillez consulter le fichier `create_labels.py` pour voir toutes les options.

Exemple de commande:
```
Python ./src/create_labels.py  < [YOUR_FILE_HERE.py] > ./data/output/labels-wh162.csv
```

# Executer le projet

Maintenant que tous les fichiers de données sont générés, ils ne restent plus qu'à démarrer l'apprentissage. Si vous avez `VS Code`, nous recommandons
d'utiliser une des commandes présentes dans le fichier `launch.json`. Cela démarre l'apprentissage avec les données de défaut.

## Arguments

Si vous ne pouvez pas utiliser les scripts de `VS Code`, le point d'entrée est le fichier `main.py`. Voici les arguments de `main.py`:

| argument | name            | type | description                                             | Default  | required |
| -------- | --------------- | ---- | ------------------------------------------------------- | -------- | -------- |
| -n       | name            | str  | RNN Model name                                          | None     | True     |
| -f       | features        | str  | Product features file location                          | None     | True     |
| -c       | context         | str  | Context features file location                          | None     | True     |
| -l       | labels          | str  | Label file location                                     | None     | False    |
| -s       | start date      | str  | Start date for the context features and products labels | None     | True     |
| -e       | end date        | str  | End date for the context features and product labels    | None     | True     |
| -hs      | historic size   | int  | Size of the historic samples                            | 7        | False    |
| -fs      | future size     | int  | Future target length                                    | 3        | False    |
| -t       | step            | int  | Size of the step between samples                        | 1        | False    |
| -st      | stack size      | int  | Number of steps to merge together before running RNN    | 1        | False    |
| -kf      | k-folds         | int  | Number of folds to do for cross validation in training  | 5        | False    |
| -bs      | Batch size      | int  | Number of features to process simultaniously            | 100      | False    |
| -ep      | epochs          | int  | Number of epoch for each run                            | 5        | False    |
| -d       | delimiter       | str  | Delimiter character for files                           | ,        | False    |
| -rw      | remove-weekends | str  | Remove weekends from training data                      | False    | False    |
| -df      | date format     | str  | Date Format of the dates in the files                   | %Y-%m-%d | False    |
| -gp      | group-products  | str  | Group product features                                  | False    | False    |
| -fc      | Filter category | int  | Filter on category                                      | None     | False    |




# Analyse graphiques de données 
La commande suivante permet de dessiner les graphiques qui ont été utilisés dans le rapport technique : 

```
python .\data_analysis.py
```

Présentement, le script n'est pas adapté pour traiter tous les formats de données possibles.

Par contre, nous avons des modules qui permettent de traiter les données afin de générer les graphiques.

Les modules utilisés afin de générer les graphiques sont les suivants : 

|Module|Fonction|
|-|-|
|`./extractor`|Extraire les données à partir d'un fichier CSV
|`./graph/plot`|Générer un graphique à partir d'un ensemble de données formatées par `extractor`


## Exemple d'utilisation
Voici un exemple d'une fonction qui permet de générer le graphique pour les données de vente de 1C Company.

- `get_total_sales_by_date_1c` permet de regrouper les ventes par jour afin de délimiter toutes les ventes quotidiennement
- `split_columns_by_year` permet de séparer en colonne les ventes regroupées annuellement 
- `plot_data_per_year_compare` permet de générer un graphique qui compare la tendance de ventes annuellement
- `plot_data_per_year_compare` permet de générer un graphique représentant une moyenne de vente quotidienne sur une ligne.
```py
def plot_1c():
    sales_1c = extractor.get_total_sales_by_date_1c()
    sales_1c = plot.split_columns_by_year(sales_1c, "item_cnt_day", start_year=2013, end_year=2015)
    plot.plot_data_per_year_compare(sales_1c, 2013, 2015, "daily total sales of 1C Company")
    plot.plot_daily_single_column(sales_1c, "mean", "average daily total sales of 1C company")

```


# Flux de processus
Le diagramme ci-dessous visualise le flux de fonctionnement de ce projet afin de faciliter la compréhension du projet.


![functional flowchart](./doc_img/functional_flow.png)


