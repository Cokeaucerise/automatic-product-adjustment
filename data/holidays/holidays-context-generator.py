#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import numpy as np
import argparse as ap
from datetime import datetime
from datetime import timedelta
import math

parser = ap.ArgumentParser(description='')

parser.add_argument('-d', '--delimiter', 	type=str,   help='Delimiter Character', 				default=',', 	        required=False)
parser.add_argument('-hd', '--holiday', 	type=str,   help='Holiday date (dd.mm)', 				default='12-31', 	    required=False)
parser.add_argument('-s', '--start', 		type=str,   help='Start date (dd.mm.yyyy)', 			default='2015-01-01', 	required=False)
parser.add_argument('-e', '--end', 		    type=str,   help='End date (dd.mm.yyyy)', 				default='2020-12-31', 	required=False)
parser.add_argument('-c', '--signoid',      type=str,   help='Convert to sinusoidal pattern (0 = no convertion, 1 = sinusoidal)', default=0, required=False)

args = parser.parse_args()

date_format = "%Y-%m-%d"

SIGNOID_OFFSET = math.pi/2

start_date = datetime.strptime(args.start, date_format)
end_date = datetime.strptime(args.end, date_format)

def main():
    sys.stderr.write(f'\n=== {__name__}: Generating holiday context values... ===\n')
    years = range(start_date.year-1, end_date.year+2)
    holidays = []
    for year in years:
        holidays.append(datetime.strptime(str(year) + "-" + args.holiday, date_format))

    result = []
    if args.signoid == 1:
        result = signoid_holiday_context(holidays)
    elif args.signoid == 0:
        result = delta_holiday_context(holidays)

    np.savetxt(sys.stdout, result, delimiter=args.delimiter, fmt='%s')

def signoid_holiday_context(holidays):
    result = []
    for i in range(0, len(holidays)-1):
        delta = delta_days(holidays[i], holidays[i+1])
        step = math.pi / delta
        for a in range(0, delta):
            date = holidays[i] + timedelta(days=a)
            if date >= start_date and date <= end_date:
                sin_value = math.sin(step * a + SIGNOID_OFFSET)
                result.append([date.strftime(date_format), sin_value])

    return result

def delta_holiday_context(holidays):
    result = []
    temp_date = start_date
    while temp_date <= end_date:
        deltas = []
        for holiday in holidays:
            deltas.append(delta_days(holiday, temp_date))

        delta = min(deltas)
        if (temp_date + timedelta(days=delta)) in holidays:
            delta = -1 * delta

        result.append([temp_date.strftime(date_format), delta])
        temp_date += timedelta(1)

    return result

def delta_days(d1, d2):
    return abs((d2 - d1).days)

main()