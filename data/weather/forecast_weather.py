#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
    Service d'ajustement des priorités de ventes automatisées

Students :
    Charles Paquin 		- PAQC03029507
    Olivier Lamarre 	- LAMO13069505
    Kyoshiro Kaizuka 	- KAIK21089205
    Maxim Diouskine 	- DIOM28049202
"""

import sys
import json
import requests
import pandas as pd
from pandas import DataFrame as df
import argparse as ap
from datetime import datetime
from get_lon_lat import get_lon_lat
from weather_dataclass import WeatherData, get_columns

OPENWEATHER_APIKEY = "ab9c2d666bed286829e2e3ca71a59431"
OPENWEATHER_URL = "https://api.openweathermap.org/data/2.5/onecall"


def get_weather_forecast(address: str) -> dict:
    """
    Get current forecast for specified address.
    THE API USED IS LIMITED TO 1,000,000 CALLS PER MONTH AND 60 CALLS PER MINUTE.

    :param address: Can be anything (postal code, city, place, full address, etc)
    :return: json with a lot of informations. Visit https://openweathermap.org/api/one-call-api
    """
    lon_lat = get_lon_lat(address)

    params = {
        "lon": str(lon_lat["x"]),
        "lat": str(lon_lat["y"]),
        "exclude": "",
        "units": "metric",
        "lang": "en",
        "appid": OPENWEATHER_APIKEY
    }

    response = requests.request("GET", OPENWEATHER_URL, params=params)
    return response.json()


def extract_daily(daily_df: pd.DataFrame) -> pd.DataFrame:
    """Extract the scalar values from inner object in dailies predictions

    Args:
        daily_df (pd.DataFrame): Original OpenWeather forecast

    Returns:
        pd.DataFrame: Scalar only forecast
    """
    daily_df['temp_day'] = [d['day'] for d in daily_df['temp']]
    daily_df['temp_min'] = [d['min'] for d in daily_df['temp']]
    daily_df['temp_max'] = [d['max'] for d in daily_df['temp']]
    daily_df['feels_like_day'] = [d['day'] for d in daily_df['feels_like']]
    daily_df['weather_main'] = [d[0]['main'] for d in daily_df['weather']]

    del daily_df['temp']
    del daily_df['feels_like']
    del daily_df['weather']

    return daily_df


def normalise_daily(data):
    list_normalised_data: WeatherData = []
    for line in data:
        normalised_data = WeatherData(
            datetime.utcfromtimestamp(line['dt']).strftime(WeatherData.DATE_FORMAT),
            (line['temp']['min'] + line['temp']['max']) / 2.0,
            line['temp']['min'],
            line['temp']['max'],
            0.0 if 'rain' not in line else line['rain'],
            0.0 if 'snow' not in line else line['snow'],
            line['wind_deg'],
            line['wind_speed'] * 3.6,  # converted to km/h
            0.0 if 'wind_gust' not in line else line['wind_gust'] * 3.6,  # converted to km/h
            line['pressure'],
            (line['sunset'] - line['sunrise']) / 60)
        list_normalised_data.append(normalised_data.to_list())

    return list_normalised_data


if __name__ == "__main__":
    parser = ap.ArgumentParser(description='Load the weather forecast data from OpenWeather.')
    parser.add_argument('-a', '--address', type=str, help='Address', default=None, required=True)
    parser.add_argument('-m', '--mode', type=str, help='Mode: ["current", "daily", "hourly", "minutely"]',
                        default='daily', required=False)
    args = parser.parse_args()

    w_data = get_weather_forecast(args.address)
    w_data_mode = w_data[str(args.mode).lower()]

    if args.mode == "daily":
        list_normalised_data = normalise_daily(w_data_mode)

        w_df = df(list_normalised_data, columns=get_columns())
    else:
        w_df = pd.read_json(json.dumps(w_data_mode))

    w_df.to_csv(sys.stdout, index=False)
    pass
