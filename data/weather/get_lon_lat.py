#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import sys
from arcgis.geocoding import geocode
from arcgis.gis import GIS

def get_lon_lat(address:str)->dict:
	gis = GIS()  # LEAVE THIS
	sys.stderr.write('Getting lon and lat...\n')

	geocode_result = geocode(address=address)

	# Take the first lon/lat find. Usually the most accurate.
	lon_lat = geocode_result[0]['location']

	sys.stderr.write('Total lon/lat found: ' + str(len(geocode_result)) + '\n')
	sys.stderr.write('First lon/lat generated: ' + str(lon_lat) + '\n')

	return lon_lat