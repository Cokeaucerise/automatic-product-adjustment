#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import sys
import json

import pandas as pd
import argparse as ap
from requests import request
from datetime import datetime
from get_lon_lat import get_lon_lat

METEOSTAT_APIKEY = "U8hdYZJsONSKst58F1R5DCHZum1qAqGC"
METEOSTAT_URL = "https://api.meteostat.net/v2/stations/daily"
METEOSTAT_DATE_FORMAT = "%Y-%m-%d"

def get_historical_weather(station:str, start_date:datetime, end_date:datetime)->dict:
	"""
	Get historical forecast.
	API IS LIMITED TO 2,000 CALLS PER DAY. BATCH IS LIMITED TO 370 DAYS.
	Visit: https://dev.meteostat.net/api/station/daily

	:param address: Can be anything (postal code, city, place, full address, etc)
	:param start_date: YYYY-MM-DD
	:param end_date: YYYY-MM-DD
	:return: json of weather for each date between start and end
	"""
	headers = { 'x-api-key': METEOSTAT_APIKEY }

	params = {
		"station": station,
		"start": start_date.strftime(METEOSTAT_DATE_FORMAT),
		"end": end_date.strftime(METEOSTAT_DATE_FORMAT)
	}

	response = request("GET", METEOSTAT_URL, params=params, headers=headers)
	if response.status_code == 200:
		return response.json()
	else:
		sys.stderr.writelines(response.content)
		return None


if __name__ == "__main__":
	parser = ap.ArgumentParser(description='Load the historical weather data form the time span from Meteostat.')
	parser.add_argument('-a', '--station', 			type=str, help='Station id', 								default=None, required=True)
	parser.add_argument('-s', '--start-date', 		type=str, help='Starting date', 							default=None, required=True)
	parser.add_argument('-e', '--end-date', 		type=str, help='End date', 									default=None, required=True)
	parser.add_argument('-df', '--date-format', 	type=str, help='Date format to use to parse input dates', 	default="%Y-%m-%d", required=False)
	args = parser.parse_args()

	start_date 	= datetime.strptime(args.start_date, args.date_format)
	end_date 	= datetime.strptime(args.end_date, args.date_format)

	w_data = get_historical_weather(args.station, start_date, end_date)
	w_data = w_data['data']
	
	w_df = pd.read_json(json.dumps(w_data))
	w_df = w_df.fillna(0)

	w_df.to_csv(sys.stdout, index=False)
	pass
