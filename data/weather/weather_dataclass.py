from dataclasses import dataclass


def get_columns():
    columns = ['DATE']
    columns += ['TAVG']
    columns += ['TMIN']
    columns += ['TMAX']
    columns += ['RAIN']
    columns += ['SNOW']
    columns += ['WDIR']
    columns += ['WSPD']
    columns += ['WPGT']
    columns += ['PRES']
    columns += ['TSUN']

    return columns


@dataclass
class WeatherData:
    DATE_FORMAT = "%Y-%m-%d"
    DATE: str  # DATE_FORMAT
    TAVG: str  # AVG TEMP IN celcius
    TMIN: str  # MIN TEMP IN celcius
    TMAX: str  # MAX TEMP IN celcius
    RAIN: str  # RAIN IN mm
    SNOW: str  # SNOW IN mm
    WDIR: str  # WIND DIRECTION IN degrees
    WSPD: str  # WIND SPEED IN km/h
    WPGT: str  # WIND PEAK GUST IN km/h
    PRES: str  # AVG SEA LEVEL AIR PRESSURE IN hPa
    TSUN: str  # TOTAL SUN TIME IN min

    def to_list(self):
        return [self.DATE, self.TAVG, self.TMIN, self.TMAX, self.RAIN, self.SNOW, self.WDIR, self.WSPD, self.WPGT,
                self.PRES, self.TSUN]
