#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

from src import extractor
from src.graph import plot
import pandas as pd
import timeit

# 1C sales data analysis
def plot_1c():
    sales_1c = extractor.get_total_sales_by_date_1c()
    sales_1c = plot.split_columns_by_year(sales_1c, "item_cnt_day", start_year=2013, end_year=2015)
    plot.plot_data_per_year_compare(sales_1c, 2013, 2015, "daily total sales of 1C Company")
    plot.plot_daily_single_column(sales_1c, "mean", "average daily total sales of 1C company")

# analyse de données météorologique
def get_weather():
    weather_data = pd.read_csv("./data/weather/features-wh82.csv")
    weather_data = plot.split_columns_by_year(weather_data, "tavg", start_year=2015, end_year=2019)

    # ajouter une colonne pour la courbe de tendance et la courbe dérivée de la tendance
    trends = []
    trends_diff = []
    for day in range(0, 365):
        trends.append(trend_temp_var(day))
        trends_diff.append(trend_temp_var_derivative(day))
    weather_data["trend"]       = trends
    weather_data["trend_diff"]  = trends_diff

    weather_data["mean_%"]          = extractor.convert_percentage(weather_data['mean'])
    weather_data["trend_%"]         = extractor.convert_percentage(weather_data['trend'])
    weather_data["trend_diff_%"]    = extractor.convert_percentage(weather_data['trend_diff'])

    return weather_data

# visualiser la variation de température météorologique
def plot_weather(weather_data):

    plot.plot_data_per_year_compare(weather_data, 2015, 2019, "daily average temprature at warehouse 82")
    plot.plot_multiple(weather_data, ["mean_%", "trend_%", "trend_diff_%"], "average daily temprature at warehouse 82")

# équation polynomiale degré 4 obtenue de Trendline de Excel
def trend_temp_var(day_of_year):
    x = day_of_year % 365
    return 0.00000003112701014861*x**4 - 0.00002532376581403420*x**3 + 0.00577180505376523000*x**2 - 0.24791490378075700000*x - 6.81245096058535000000

# dérivée de l'équation de Trendline de Excel
def trend_temp_var_derivative(day_of_year):
    x = day_of_year % 365
    return (60602025754188885435918304522512000*x**3-36977648207974769418501132725059765500*x**2+5618642408096127697186318199151266679792*x-120668073419505855330089569336355395924125)/486731824425603507939973549912837416249000

# analyse de données CDMV
# construire un ensemble de données de ventes des produits antiparasitaires entre 2015 et 2019 
def get_cdmv_data():
    print("ceci peut prendre quelques minutes...")
    extractor.generate_joined_sales_item_cdmv(start_year=2015, end_year=2019, warehouse_id=82, category_filter="Antiparasitaires PA")
    sales_cdmv = extractor.get_total_sales_by_date_cdmv()

    return sales_cdmv

# analyse de données CDMV
def plot_cdmv(sales_cdmv):
    plot.plot_daily_single_column(sales_cdmv, "mean_%", "daily average sales")

def plot_all():
    plot_1c()

    weather_data = get_weather()
    plot_weather(weather_data)

    sales_cdmv = get_cdmv_data()
    plot_cdmv(sales_cdmv)
    
    combined = weather_data["sales_%"] = sales_cdmv["mean_%"]
    plot.plot_multiple(weather_data, ["mean_%", "trend_%", "trend_diff_%", "sales_%"], "average temprature vs sales on %")

    print(weather_data)
    print(sales_cdmv)


if __name__ == "__main__":
    plot_all()