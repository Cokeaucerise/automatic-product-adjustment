#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np
import pandas as pd
import argparse as ap
import multiprocessing as mp
from src.rnn import rnn
from datetime import datetime, timedelta
from src.multivariate import multivariate_data_combination
from sklearn.preprocessing import minmax_scale
from scipy.misc import derivative

def multivariate_rnn_train(	name:str, p_features:np.ndarray, c_feaures:np.ndarray, labels:pd.DataFrame,
							start_date:str, end_date:str, history:int, future:int, step:int,
							stack_size:int, k_folds:int, batch_size:int, epochs:int,
							remove_weekends:bool=False, date_format:str='%d.%m.%Y'):
	"""
	Main entry point to order data and train or make prediction with RNN

	Args:
		name (str): Name of the model to use or generate
		p_features (np.ndarray): products features
		c_feaures (np.ndarray): Context features
		labels (pd.DataFrame): labels order by product (row) & dates (cols)
		start_date (str): Starting date of the dataset to use
		end_date (str): End date of the dataset to use
		history (int): Size of the history to take in account for the LSTM
		future (int): Size of the future prediction to make
		step (int): Size of the steps to take in between samples in the dataset
		stack_size (int): Number of samples to stack together before starting RNN
		metrics_group_size (float): Size of the group for the metrics confusion matrix
		batch_size (int): Batch size of the RNN
		date_format (str, optional): String format of dates. Defaults to '%d.%m.%Y'.
	"""
	# GET LIST OF DATES FOR TRAINING
	start_date 	= datetime.strptime(start_date, date_format)
	end_date 	= datetime.strptime(end_date, date_format)

	dates_diff = end_date - start_date
	dates_list = [start_date + timedelta(days=x) for x in range(dates_diff.days + 1)]

	if remove_weekends == str(True): dates_list = [d for d in dates_list if d.weekday() < 5]
	
	dates_list = [d.strftime(date_format) for d in dates_list]

	start_i = history
	end_i 	= len(dates_list) - (future if labels is not None else 0)
	steps 	= range(start_i, end_i, step)

	stack_size = stack_size if stack_size > 0 else len(steps)
	stack_range = max(1, int(len(steps)/stack_size))
	stacked_steps = [steps[i*stack_size:(i+1)*stack_size] for i in range(stack_range)]

	for stack in stacked_steps:
		sys.stderr.write(f'\n=== Training stack {stacked_steps.index(stack) + 1}/{len(stacked_steps)} ===\n')

		x, y, ids, pred_dates = multivariate_data_combination(p_features, c_features, dates_list, stack, history, future, labels=labels)
		predictions = rnn(name, x, y=y, batch_size=batch_size, epochs=epochs, k_folds=k_folds)

		if predictions is not None:

			pred_df = pd.DataFrame(ids, columns=['item_id'])

			for i in range(len(pred_dates)): # range(0, predictions.shape[0], len(ids)):
				pred_step = i * ids.shape[0]
				pred_slice = predictions[pred_step : pred_step + ids.shape[0]]
				slice_dates = pred_dates[i]

				for slice_col in range(pred_slice.shape[1]):
					pred_df[slice_dates[slice_col]] = pred_slice[:,slice_col]
					pass

				pass
			
			first_date = pred_dates[0,0]
			last_date = pred_dates[pred_dates.shape[0] - 1, pred_dates.shape[1] - 1]
			pred_df.to_csv(f'./predictions/{name}-{first_date}-{last_date}.csv', sep=',', index=False)

		pass
	pass

if __name__ == "__main__":
	parser = ap.ArgumentParser(description='')
	parser.add_argument('-n', '--name', 			type=str, help='RNN Model name', 											default=None, 		required=True)
	parser.add_argument('-f', '--features', 		type=str, help='Product features file', 									default=None, 		required=True)
	parser.add_argument('-c', '--context', 			type=str, help='Context features file', 									default=None, 		required=True)
	parser.add_argument('-l', '--labels', 			type=str, help='labels file', 												default=None, 		required=False)
	parser.add_argument('-s', '--start-date', 		type=str, help='Starting date for the context features and product labels', default=None, 		required=True)
	parser.add_argument('-e', '--end-date', 		type=str, help='End date for the context features and product labels', 		default=None, 		required=True)
	parser.add_argument('-hs','--history-size', 	type=int, help='Size of the historic samples', 								default=7, 			required=False)
	parser.add_argument('-fs','--future-size', 		type=int, help='Future target length', 										default=3, 			required=False)
	parser.add_argument('-t', '--step', 			type=int, help='Size of the step between samples', 							default=1, 			required=False)
	parser.add_argument('-st', '--stack-size', 		type=int, help='Number of steps to merge together before running RNN', 		default=-1, 		required=False)
	parser.add_argument('-kf', '--k-folds', 		type=int, help='Number of folds to do for cross validation in training', 	default=5, 			required=False)
	parser.add_argument('-bs', '--batch-size', 		type=int, help='Number of features to process simultaniously', 				default=100, 		required=False)
	parser.add_argument('-ep', '--epochs', 			type=int, help='Number of epoch for each run', 								default=5, 			required=False)
	parser.add_argument('-d', '--delimiter', 		type=str, help='Delimiter character', 										default=',', 		required=False)
	parser.add_argument('-rw', '--remove-weekends', type=str, help='Remove weekends from training data', 						default='False', 	required=False)
	parser.add_argument('-df', '--date-format', 	type=str, help='Date format', 												default='%Y-%m-%d', required=False)
	parser.add_argument('-gp', '--group-products', 	type=str, help='Group product features', 									default='False', 	required=False)
	parser.add_argument('-fc', '--filter-category', type=int, help='Filter on category', 										default=None, 		required=False)
	args = parser.parse_args()

	p_features 			= pd.read_csv(args.features)
	c_features 			= pd.read_csv(args.context).to_numpy()
	c_features[:,1:] 	= minmax_scale(c_features[:,1:])

	labels = None
	if args.labels is not None:
		labels = pd.read_csv(args.labels)

	if args.filter_category is not None:
		p_features = p_features[p_features['cat'] == args.filter_category]
		if labels is not None:
			labels = labels[[item in p_features['id'].to_numpy() for item in labels['item_id']]]

	if args.group_products == str(True):
		if labels is not None:
			labels = pd.DataFrame([*labels.sum().to_numpy()[1:]], columns=labels.columns)
			labels['item_id'] = 1
		p_features = np.array([1]).reshape((labels.shape[0],1))

	p_features = p_features.to_numpy()

	multivariate_rnn_train(
		args.name,
		p_features, c_features, labels,
		start_date=args.start_date, end_date=args.end_date,
		history=args.history_size, future=args.future_size, step=args.step,
		stack_size=args.stack_size, k_folds=args.k_folds, batch_size=args.batch_size, epochs=args.epochs,
		remove_weekends=args.remove_weekends, date_format=args.date_format
	)
	pass
