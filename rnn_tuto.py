#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

# Time series forecasting tutorial by Tensorflow
# Predict temprature
# https://www.tensorflow.org/tutorials/structured_data/time_series

import tensorflow as tf

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

mpl.rcParams['figure.figsize'] = (8, 6)
mpl.rcParams['axes.grid'] = False

# The weather dataset
# This dataset contains 14 different features such as air temperature, atmospheric pressure, 
# and humidity. These were collected every 10 minutes, beginning in 2003. 
# For efficiency, you will use only the data collected between 2009 and 2016. 
# zip_path = tf.keras.utils.get_file(
#     origin='https://storage.googleapis.com/tensorflow/tf-keras-datasets/jena_climate_2009_2016.csv.zip',
#     fname='jena_climate_2009_2016.csv.zip',
#     extract=True)
# csv_path, _ = os.path.splitext(zip_path)

df = pd.read_csv('./data/weather/features-wh82.csv')

def print_weather_dataframe():
    print("printing weather data frame...")
    print(df.head())


############################## Forecast a univariable time series ##############################

# the first 300,000 rows of the data will be the training dataset, 
# and there remaining will be the validation dataset. 
# This amounts to ~2100 days worth of training data.
TRAIN_SPLIT = 1500

# Setting seed to ensure reproducibility.
tf.random.set_seed(13)

# extract only the temperature from the dataset.
uni_temp_dates = df['tavg']
uni_temp_dates.index = df['date']

# print and plot extracted data 
def show_temprature_dataframe():
    print("printing temprature data frame...")
    print(uni_temp_dates.head()) # pandas.DataFrame.head returns the first n rows for the object based on position. default=5
    uni_temp_dates.plot(subplots=True)
    plt.show()

uni_temp = uni_temp_dates.values

# The mean and standard deviation should only be computed using the training data.
uni_train_mean = uni_temp[:TRAIN_SPLIT].mean()
uni_train_std = uni_temp[:TRAIN_SPLIT].std()

# standardize the data
uni_data = (uni_temp-uni_train_mean)/uni_train_std


# The function below returns the above described windows of time for the model to train on. 
# The parameter history_size is the size of the past window of information. 
# The target_size is how far in the future does the model need to learn to predict. 
# The target_size is the label that needs to be predicted.
def univariate_data(dataset, start_index, end_index, history_size, target_size):
  data = []
  labels = []

  start_index = start_index + history_size
  if end_index is None:
    end_index = len(dataset) - target_size

  for i in range(start_index, end_index):
    indices = range(i-history_size, i)
    # Reshape data from (history_size,) to (history_size, 1)
    data.append(np.reshape(dataset[indices], (history_size, 1)))
    labels.append(dataset[i+target_size])
  return np.array(data), np.array(labels)

univariate_past_history = 20
univariate_future_target = 0

# the first 300,000 rows of the data will be the training dataset
x_train_uni, y_train_uni = univariate_data(uni_data, 0, TRAIN_SPLIT, univariate_past_history, univariate_future_target)
# the remaining will be the validation dataset
x_val_uni, y_val_uni = univariate_data(uni_data, TRAIN_SPLIT, None, univariate_past_history, univariate_future_target)

def print_univariate_data():
    print ('Single window of past history')
    print (x_train_uni[0])
    print ('\n Target temperature to predict')
    print (y_train_uni[0])

# The information given to the network is given in blue, and it must predict the value at the red cross
def create_time_steps(length):
  return list(range(-length, 0))

# make plottable object 
def make_plot(plot_data, delta, title):
  labels = ['History', 'True Future', 'Model Prediction']
  marker = ['.-', 'rx', 'go'] # dotted line, red cross, green circle?
  time_steps = create_time_steps(plot_data[0].shape[0])
  if delta:
    future = delta
  else:
    future = 0

  plt.title(title)
  for i, x in enumerate(plot_data):
    if i:
      plt.plot(future, plot_data[i], marker[i], markersize=10, label=labels[i])
    else:
      plt.plot(time_steps, plot_data[i].flatten(), marker[i], label=labels[i])
  plt.legend()
  plt.xlim([time_steps[0], (future+5)*2])
  plt.xlabel('Time-Step')
  return plt


############################## BASELINE ##############################

# starting point used for comparisons
def baseline(history):
    return np.mean(history)

def show_training_plot():
    print("plotting training data...")
    #make_plot([x_train_uni[0], y_train_uni[0]], 0, 'Sample Example').show()
    make_plot([x_train_uni[0], y_train_uni[0], baseline(x_train_uni[0])], 0, 'Baseline Prediction Example').show()


############################## RNN ##############################

BATCH_SIZE = 100
BUFFER_SIZE = 1000

# tf.data to shuffle, batch, and cache the dataset
train_univariate = tf.data.Dataset.from_tensor_slices((x_train_uni, y_train_uni))
train_univariate = train_univariate.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

val_univariate = tf.data.Dataset.from_tensor_slices((x_val_uni, y_val_uni))
val_univariate = val_univariate.batch(BATCH_SIZE).repeat()

simple_lstm_model = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=x_train_uni.shape[-2:]),
    tf.keras.layers.Dense(1)
])

# optimizer adam : algorithm for first-order gradient-based optimization of stochastic objective functions
# loss mae : Mean absolute error
simple_lstm_model.compile(optimizer='adam', loss='mae')

def print_sample_prediction():
    for x, y in val_univariate.take(1):
        print(simple_lstm_model.predict(x).shape)

# Model training
EVALUATION_INTERVAL = 200 # in the interest of saving time, each epoch will only run for 200 steps
EPOCHS = 10

# Predict using the simple LSTM model
def show_univariable_prediction():
    simple_lstm_model.fit(train_univariate, epochs=EPOCHS, steps_per_epoch=EVALUATION_INTERVAL, validation_data=val_univariate, validation_steps=50)
    for x, y in val_univariate.take(3):
        plot = make_plot([x[0].numpy(), 
                            y[0].numpy(), 
                            simple_lstm_model.predict(x)[0]], 
                            0, 
                            'Simple LSTM model')
        plot.show()


############################## Forecast a multivariate time series ##############################
# https://www.tensorflow.org/tutorials/structured_data/time_series#part_2_forecast_a_multivariate_time_series

features_considered = ['tavg', 'tmin', 'tmax']

features = df[features_considered]
features.index = df['date']
features.head()
features.plot(subplots=True)

def show_multivariate_data():
  plt.show()

# again, standardize the dataset using the mean and standard deviation of the training data
dataset = features.values
data_mean = dataset[:TRAIN_SPLIT].mean(axis=0)
data_std = dataset[:TRAIN_SPLIT].std(axis=0)

dataset = (dataset-data_mean)/data_std


def multivariate_data(dataset, target, start_index, end_index, history_size, target_size, step, single_step=False):
  data = []
  labels = []

  start_index = start_index + history_size
  if end_index is None:
    end_index = len(dataset) - target_size

  for i in range(start_index, end_index):
    indices = range(i-history_size, i, step)
    data.append(dataset[indices])

    if single_step:
      labels.append(target[i+target_size])
    else:
      labels.append(target[i:i+target_size])

  return np.array(data), np.array(labels)

# 720 observations every hour
# sampling every hour : 24/day
# data from 5 days so 24 * 5 = 120 observations
# datapoint : 12 hours into futur
# use 12 * 6 = 72 observations
past_history = 28
future_target = 7
STEP = 1

x_train_single, y_train_single = multivariate_data(dataset, dataset[:, 1], 0,
                                                   TRAIN_SPLIT, past_history,
                                                   future_target, STEP,
                                                   single_step=True)
x_val_single, y_val_single = multivariate_data(dataset, dataset[:, 1],
                                               TRAIN_SPLIT, None, past_history,
                                               future_target, STEP,
                                               single_step=True)

def print_multi_single_data_point():
    print ('Single window of past history : {}'.format(x_train_single[0].shape))

train_data_single = tf.data.Dataset.from_tensor_slices((x_train_single, y_train_single))
train_data_single = train_data_single.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

val_data_single = tf.data.Dataset.from_tensor_slices((x_val_single, y_val_single))
val_data_single = val_data_single.batch(BATCH_SIZE).repeat()

single_step_model = tf.keras.models.Sequential()
single_step_model.add(tf.keras.layers.LSTM(32,
                                           input_shape=x_train_single.shape[-2:]))
single_step_model.add(tf.keras.layers.Dense(1))

single_step_model.compile(optimizer=tf.keras.optimizers.RMSprop(), loss='mae')

# sample prediction
def print_multi_sample_prediction():
  for x, y in val_data_single.take(1):
    print(single_step_model.predict(x).shape)

single_step_history = single_step_model.fit(train_data_single, epochs=EPOCHS,
                                            steps_per_epoch=EVALUATION_INTERVAL,
                                            validation_data=val_data_single,
                                            validation_steps=50)
                                  
def plot_train_history(history, title):
  loss = history.history['loss']
  val_loss = history.history['val_loss']

  epochs = range(len(loss))

  plt.figure()

  plt.plot(epochs, loss, 'b', label='Training loss')
  plt.plot(epochs, val_loss, 'r', label='Validation loss')
  plt.title(title)
  plt.legend()

  plt.show()

def show_train_history_plot():
  plot_train_history(single_step_history,'Single Step Training and validation loss')

# Predict a single step future
def show_simple_step_prediction():
  for x, y in val_data_single.take(3):
    plot = make_plot([x[0][:, 1].numpy(), y[0].numpy(), single_step_model.predict(x)[0]], 12, 'Single Step Prediction')
    plot.show()

# Multi-Step model : predict a sequence of the future
future_target = 72
x_train_multi, y_train_multi = multivariate_data(dataset, dataset[:, 1], 0,
                                                 TRAIN_SPLIT, past_history,
                                                 future_target, STEP)
x_val_multi, y_val_multi = multivariate_data(dataset, dataset[:, 1],
                                             TRAIN_SPLIT, None, past_history,
                                             future_target, STEP)

def print_multi_sample_datapoint():
  print ('Single window of past history : {}'.format(x_train_multi[0].shape))
  print ('\n Target temperature to predict : {}'.format(y_train_multi[0].shape))

train_data_multi = tf.data.Dataset.from_tensor_slices((x_train_multi, y_train_multi))
train_data_multi = train_data_multi.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

val_data_multi = tf.data.Dataset.from_tensor_slices((x_val_multi, y_val_multi))
val_data_multi = val_data_multi.batch(BATCH_SIZE).repeat()

# the history and the future data are sampled every hour
def multi_step_plot(history, true_future, prediction):
  plt.figure(figsize=(12, 6))
  num_in = create_time_steps(len(history))
  num_out = len(true_future)

  plt.plot(num_in, np.array(history[:, 1]), label='History')
  plt.plot(np.arange(num_out)/STEP, np.array(true_future), 'bo',
           label='True Future')
  if prediction.any():
    plt.plot(np.arange(num_out)/STEP, np.array(prediction), 'ro',
             label='Predicted Future')
  plt.legend(loc='upper left')
  plt.show()

def show_multi_step_plot():
  for x, y in train_data_multi.take(1):
    multi_step_plot(x[0], y[0], np.array([0]))

# Since the task here is a bit more complicated than the previous task, 
# the model now consists of two LSTM layers. 
# Finally, since 72 predictions are made, the dense layer outputs 72 predictions.
multi_step_model = tf.keras.models.Sequential()
multi_step_model.add(tf.keras.layers.LSTM(32,
                                          return_sequences=True,
                                          input_shape=x_train_multi.shape[-2:]))
multi_step_model.add(tf.keras.layers.LSTM(16, activation='relu'))
multi_step_model.add(tf.keras.layers.Dense(72))

multi_step_model.compile(optimizer=tf.keras.optimizers.RMSprop(clipvalue=1.0), loss='mae')

def print_model_prediction_before_train():
  for x, y in val_data_multi.take(1):
    print (multi_step_model.predict(x).shape)

multi_step_history = multi_step_model.fit(train_data_multi, epochs=EPOCHS,
                                          steps_per_epoch=EVALUATION_INTERVAL,
                                          validation_data=val_data_multi,
                                          validation_steps=50)

plot_train_history(multi_step_history, 'Multi-Step Training and validation loss')


# Predict a multi-step future
def show_multi_step_prediction():
  for x, y in val_data_multi.take(3):
    multi_step_plot(x[0], y[0], multi_step_model.predict(x)[0])
