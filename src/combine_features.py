#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import tqdm
import numpy as np
import multiprocessing as mp
from datetime import datetime
from src.get_rows import get_rows
from src.get_cols import get_cols

def combine_features(	p_features:np.ndarray, c_features:np.ndarray, labels:np.ndarray=None, dtype:np.dtype=np.float,
						include_date_features:bool=False, parallel:bool=False, verbose:bool=False, date_format:str='%Y-%m-%d',)->np.ndarray:
	"""
	Combine 2 features set together by adding every context features to expended products features.

	Args:
		p_features (np.ndarray): Product features
		c_features (np.ndarray): Context features
		dtype (np.dtype, optional): Type of the output dataset. Defaults to np.float.
		parallel (bool, optional): Execute in parallel. Defaults to False.
		verbose (bool, optional): Print out execution details. Defaults to False.

	Returns:
		np.ndarray: Combined features (n_product_features, n_context_features, features)
	"""
	
	timesteps = c_features.shape[0]
	p_features_rows = p_features.shape[0]
	p_features_cols = p_features.shape[1] - 1 # Remove id
	c_features_cols = c_features.shape[1] - 1 # Remove id
	features_cols = 1 + p_features_cols + c_features_cols
	combined_shape = (p_features_rows, timesteps, features_cols)

	if include_date_features:
		dates = list(map(lambda d: datetime.strptime(d,date_format), c_features[:,0]))
		dates_features = list(map(lambda d: [d.weekday(), d.day, d.month], dates))
		c_features = np.concatenate((c_features, dates_features), axis=1)
		pass

	if verbose:
		sys.stderr.write(f'\n=== {__name__}: Combining products features with context features\n')
		sys.stderr.write(f'\n=== {__name__}: Product features shape: {p_features.shape}\n')
		sys.stderr.write(f'\n=== {__name__}: Context features shape: {c_features.shape}\n')
		sys.stderr.write(f'\n=== {__name__}: Combined shape: {combined_shape}\n')

	if parallel:
		pool = mp.Pool(mp.cpu_count())
		pbar = tqdm.tqdm(total=p_features_rows)
		tasks = [pool.apply_async(__combine_product_context, args=(p_f, c_features), callback=lambda _: pbar.update(1)) for p_f in p_features]
		pool.close()
		pool.join()

		results = np.array([t.get() for t in tasks], dtype=dtype)
	else:
		results = np.array([__combine_product_context(p_f, c_features) for p_f in p_features], dtype=dtype)

	if results.shape[0] > 0:
		results = results[results[:,0,0].argsort()]

	return results

def __combine_product_context(p_features:np.ndarray, c_features:np.ndarray, labels:np.ndarray=None)->np.ndarray:
	"""
	Append every context features to a single product features row

	Args:
		p_features (np.ndarray): Single product features
		c_features (np.ndarray): Context features

	Returns:
		np.ndarray: [description]
	"""
	p_features_repeat = np.repeat([p_features], repeats=c_features.shape[0], axis=0)

	if labels is not None:
		p_labels = labels[labels['item_id'] == p_features[0]]
		p_labels = get_cols(p_labels, c_features[:,0]).transpose()
		p_features_repeat = np.concatenate((p_features_repeat, p_labels), axis=1)

	p_features_repeat = np.concatenate((p_features_repeat, c_features[:,1:]), axis=1)
	
	return p_features_repeat