#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import tqdm
import numpy as np
import pandas as pd
import argparse as ap
import scipy.stats as st
import multiprocessing as mp
from pandas import DataFrame
from datetime import datetime, timedelta
from sklearn.preprocessing import normalize, minmax_scale, quantile_transform, scale, KBinsDiscretizer

def create_labels(	sales:DataFrame,
					remove_outliers:bool=True, clamp_outliers:bool=True,
					normalize_values:bool=True, quantile:bool=True,
					zero_min_values:bool=True, minmax_values:bool=True, date_fmt:str='%d.%m.%Y')->pd.DataFrame:
	"""
	Create item value label from the sales data
	"""

	sys.stderr.write(f'\n=== {__name__}: Grouping by date ===\n')
	grouped_by_id = sales.groupby(['item_id'])

	start_date 	= datetime.strptime(sales['date'].min(), date_fmt)
	end_date 	= datetime.strptime(sales['date'].max(), date_fmt)

	dates_diff = end_date - start_date
	dates = [start_date + timedelta(days=x) for x in range(dates_diff.days + 1)]
	dates = np.array([d.strftime(date_fmt) for d in dates])
	dates = dates[dates.argsort()]

	dates_df = DataFrame({'date': dates})

	sys.stderr.write(f'\n=== {__name__}: Generating labels ===\n')
	pool = mp.Pool(mp.cpu_count())
	pbar = tqdm.tqdm(total=len(grouped_by_id))
	tasks = [pool.apply_async(compute_single_label, args=(k, gp, dates_df), callback=lambda _: pbar.update(1)) for k, gp in grouped_by_id]
	pool.close()
	pool.join()

	results = np.array([t.get() for t in tasks], dtype=np.float)

	ids = results[:,0]
	ids = ids.reshape(ids.shape[0],1)

	values = np.nan_to_num(results[:,1:], copy=False, nan=0.0, posinf=0.0, neginf=0.0)

	sys.stderr.write(f'\n=== {__name__}: Starting Post Preocessing results ===\n')

	if remove_outliers:
		sys.stderr.write(f'\n=== {__name__}: Remove outliers results ===\n')
		z = st.zscore(values, axis=None, ddof=0.0)
		values[z > 3] = 0
	
	if clamp_outliers:
		sys.stderr.write(f'\n=== {__name__}: Clamp outliers results ===\n')
		# Replace positive outlier values with the next available max to clip the
		# max values under the initial zscore of value 3
		values[z > 3] = np.max(values)

	if normalize_values:
		sys.stderr.write(f'\n=== {__name__}: Normalizing results ===\n')
		values = normalize(values, copy=False)

	if quantile:
		sys.stderr.write(f'\n=== {__name__}: Quantile transform results ===\n')
		values = quantile_transform(values, axis=1, n_quantiles=100, copy=False)

	if zero_min_values:
		sys.stderr.write(f'\n=== {__name__}: Scaling results ===\n')
		values_diff = np.max(values) - np.min(values)
		values = minmax_scale(values, axis=1, feature_range=(0,values_diff), copy=False) # scale to logistic curve

	if minmax_values:
		sys.stderr.write(f'\n=== {__name__}: Scaling results ===\n')
		values = minmax_scale(values, axis=1, feature_range=(0,1), copy=False) # scale to logistic curve

	sys.stderr.write(f'\n=== {__name__}: Writing results ===\n')

	values = np.nan_to_num(values, copy=False, posinf=0.0, neginf=0.0)
	values = np.round(values, decimals=2)

	results = np.hstack((ids, values))
	results = results[results[:,0].argsort()]

	return pd.DataFrame(results, columns=['item_id', *dates_df['date']])

def compute_single_label(k:int, gp:DataFrame=None, dates:DataFrame=None, values:list=[]):

	# Fill the item dates with empty dates
	gp = pd.concat([gp, dates], join='outer',).fillna(0)

	values = gp.groupby(['date'])['count'].sum()

	# Add the item id as first column for identification
	return  [k, *values.to_numpy()]

if __name__ == "__main__":
	#Argument parser
	parser = ap.ArgumentParser(description='Create the labels from a sales file with cols: ["item_id", "date", "count", "warehouse_id"]')
	parser.add_argument('-wf', '--warehouse-filter', type=int, help='Filter on warehouse value', 	default=None, 		required=False)
	parser.add_argument('-in', '--ignore-negative', type=bool, help='Ignore negative quantities', 	default=True, 		required=False)
	parser.add_argument('-id', '--i-delimiter', 	type=str, help='Input delimiter Character', 	default='\t', 		required=False)
	parser.add_argument('-od', '--o-delimiter', 	type=str, help='Ouput delimiter Character', 	default=',', 		required=False)
	parser.add_argument('-ro', '--remove-outlier', 	type=str, help='Remove outlier', 				default='True', 	required=False)
	parser.add_argument('-no', '--normalize', 		type=str, help='Normalize', 					default='True', 	required=False)
	parser.add_argument('-co', '--clamp-outlier', 	type=str, help='Clamp outlier', 				default='False', 	required=False)
	parser.add_argument('-qt', '--quantile', 		type=str, help='Quantile', 						default='False', 	required=False)
	parser.add_argument('-zm', '--zero-min', 		type=str, help='Set the min to zero', 			default='False', 	required=False)
	parser.add_argument('-mm', '--min-max', 		type=str, help='Min Max', 						default='False', 	required=False)
	parser.add_argument('-df', '--date-format', 	type=str, help='Dates format', 					default='%Y-%m-%d', required=False)
	args = parser.parse_args()

	sys.stderr.write(f'\n=== {__name__}: Parsing data frame ===\n')

	sales = pd.read_csv(sys.stdin, sep=args.i_delimiter, usecols=['item_id', 'date', 'count', 'warehouse_id'])
	sales = sales[['item_id', 'date', 'count', 'warehouse_id']]

	sales = sales.fillna(0)

	if args.ignore_negative is True:
		sales['count'] = sales['count'].clip(lower=0)

	if args.warehouse_filter is not None:
		sales = sales[(sales.warehouse_id == args.warehouse_filter)]

	labels = create_labels(	sales,
							remove_outliers = 	args.remove_outlier == str(True),
							clamp_outliers = 	args.clamp_outlier == str(True),
							normalize_values = 	args.normalize == str(True),
							quantile = 			args.quantile == str(True),
							zero_min_values = 	args.zero_min == str(True),
							minmax_values = 	args.min_max == str(True),
							date_fmt=args.date_format)
	labels.to_csv(sys.stdout, sep=args.o_delimiter, index=False)
	pass