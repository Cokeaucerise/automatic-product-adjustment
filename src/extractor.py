#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
from os.path import abspath
import numpy as np
import sys
import datetime
import numpy as np
import pandas as pd
from os.path import abspath

ITEM_DESCRIPTION_INDEX = 3
ITEM_CATEGORY_INDEX = 2
DATA_FOLDER = abspath(os.path.join(__name__, "../data/"))
OUTPUT_FOLDER = abspath(os.path.join(__name__, "../output/"))

np.set_printoptions(threshold=sys.maxsize)
def get_data():
    dataList = []
    data_paths = os.listdir(DATA_FOLDER)
    for file_path in data_paths:
        csv_extract = pd.read_csv(os.path.join(DATA_FOLDER, file_path), low_memory=False)
        dataList.append(csv_extract.to_numpy())
    return dataList

"""Point d entree"""
def get_primary_data():
    """Get tout les sales"""
    csv_sales_data = pd.read_csv(os.path.join(DATA_FOLDER, "sales_train.csv"), low_memory=False)
    sales_data = csv_sales_data.to_numpy()

    """Get toutes les infos des items"""
    csv_items_information = pd.read_csv(os.path.join(DATA_FOLDER, "items.csv"), low_memory=False)
    items_information = csv_items_information.to_numpy()

    categories_one_shot = __get_one_shot_categories(items_information, sales_data)
    res_array = np.append(sales_data, categories_one_shot, axis=1)
    # TODO A decommenter quand on va utiliser le pipeline. Pour l instant on retourne le array
    # save_numpy_to_csv(res_array)
    return res_array


def __get_one_shot_categories(items_information_data, sales_data):
    """Get categories"""
    csv_categories = pd.read_csv(os.path.join(DATA_FOLDER, "item_categories.csv"), low_memory=False)
    categories_data = csv_categories.to_numpy()

    """Creer un tableau de grandeur X Y. Dont x est le nombre de vente et Y le nombre de categories"""
    categories_one_shot = np.zeros((len(sales_data), len(categories_data)))


    for index in range(len(sales_data)):
        item_desc = items_information_data[sales_data[index][ITEM_DESCRIPTION_INDEX]]
        categories_one_shot[index][item_desc[ITEM_CATEGORY_INDEX]] = 1

    return categories_one_shot


"""Point d entree"""
def get_primary_data():
    """Get tout les sales"""
    csv_sales_data = pd.read_csv(os.path.join(DATA_FOLDER, "sales_train.csv"), low_memory=False)
    sales_data = csv_sales_data.to_numpy()

    """Get toutes les infos des items"""
    csv_items_information = pd.read_csv(os.path.join(DATA_FOLDER, "items.csv"), low_memory=False)
    items_information = csv_items_information.to_numpy()

    #categories_one_shot = __get_one_shot_categories(items_information, sales_data)
    #res_array = np.append(sales_data, categories_one_shot, axis=1)
    fmt_array = __get_frmt_array(sales_data[0])
    # TODO A decommenter quand on va utiliser le pipeline. Pour l instant on retourne le array
    np.savetxt(sys.stdout, sales_data, fmt=fmt_array, delimiter=',')

def __get_one_shot_categories(items_information_data, sales_data):
    """Get categories"""
    csv_categories = pd.read_csv(os.path.join(DATA_FOLDER, "item_categories.csv"), low_memory=False)
    categories_data = csv_categories.to_numpy()

    """Creer un tableau de grandeur X Y. Dont x est le nombre de vente et Y le nombre de categories"""
    categories_one_shot = np.zeros((len(sales_data), len(categories_data)))


    for index in range(len(sales_data)):
        item_desc = items_information_data[sales_data[index][ITEM_DESCRIPTION_INDEX]]
        categories_one_shot[index][item_desc[ITEM_CATEGORY_INDEX]] = 1

    return categories_one_shot


def __get_frmt_array(data_array):
    frmt_array =[]
    for index in range(len(data_array)):
        if isinstance(data_array[index], int):
            frmt_array.append('%i')
        elif isinstance(data_array[index], float):
            frmt_array.append('%.2f')
        else:
            frmt_array.append('%s')
    return frmt_array


def get_total_sales_by_date_1c():
    path = os.path.join(DATA_FOLDER, 'client/TRAIN/sales_train.csv')
    data = pd.read_csv(path)
    data['date'] = pd.to_datetime(data['date'])  # convert date str column to datetime
    pos_sales = data[data['item_cnt_day'] > 0]  # get only sales count > 0

    # pos_sales = pos_sales[pos_sales['date'] >= '2013-01-01'] # filter 2013
    # pos_sales = pos_sales[pos_sales['date'] <= '2013-12-31'] # filter 2013

    grouped = pos_sales.groupby('date', as_index=False).sum()[
        ['date', 'item_cnt_day', 'item_price']]  # calculate sum grouping by date, select only date and item count
    return grouped

def generate_joined_sales_item_cdmv(start_year=2015, end_year=2015, warehouse_id=-1, category_filter=""):
    """
        Joindre les détails des items pour chaque ligne des ventes

        l'en-tête de colonnes semblerait voici : 
        Invoice_Line_Id Invoice_Date  Item_Id  ...  Quantity_Invoiced  ... Accounting_Regrouping_Category ...
              111111111   2015-01-05    12345  ...                  1  ...            Antiparasitaires PA ...

        Enfin, sauvegarder le nouveau dataset sur /output/cdmv_sales_item_20XX.csv pour chaque année
    """

    path = os.path.join(DATA_FOLDER, 'cdmv/Item.tsv')
    item_details = pd.read_csv(path, sep='\t')

    for year in range(start_year, end_year +1):
        year_str = str(year)

        path = os.path.join(DATA_FOLDER, "cdmv/Ventes " + year_str + ".tsv")

        output_path = os.path.join(OUTPUT_FOLDER, "cdmv_sales_item_" + year_str + ".tsv")

        # skip si le fichier est déjà généré
        if not os.path.exists(output_path):

            sales = pd.read_csv(path, sep='\t')

            # filter by warehouse
            if (warehouse_id >= 0):
                sales = sales[sales["Warehouse_Id"]==warehouse_id]

            joined = pd.merge(sales, item_details, how="left", on=["Item_Id", "Item_Id"])

            # filter by category
            if (category_filter != ""):
                joined = joined[joined["Accounting_Regrouping_Category"]==category_filter]

            joined.to_csv(output_path, index=False, sep='\t')


def get_total_sales_by_date_cdmv():
    combined_data_path = os.path.join(OUTPUT_FOLDER, "cdmv_total_sales_splitted")

    if (os.path.exists(combined_data_path)):
        data = pd.read_csv(combined_data_path, sep='\t')
        return data

    days = pd.date_range("2019-01-01", "2019-12-31")
    combined = pd.DataFrame({"date":days.strftime("%m-%d")}) # obtenir une colonne de date en forme de 01-01

    for year in range(2015, 2020):
        year_str = str(year)
        
        output_path = os.path.join(OUTPUT_FOLDER, "cdmv_total_daily_sales_" + year_str + ".tsv")

        # skip si le fichier est déjà généré
        if not os.path.exists(output_path):

            path = os.path.join(OUTPUT_FOLDER, "cdmv_sales_item_" + year_str + ".tsv")
            data = pd.read_csv(path, sep='\t') # sep t for tsv file

            data['Invoice_Date'] = pd.to_datetime(data['Invoice_Date'])  # convert date str column to datetime
            pos_sales = data[data["Quantity_Invoiced"] > 0]  # get only sales count > 0

            # regrouper et calculer la somme de la ventes par jour
            data = pos_sales.groupby('Invoice_Date', as_index=False).sum()[['Invoice_Date', "Quantity_Invoiced"]]

            # remplir les lignes manquantes avec la valeur 0 (e.g. il y a la vente au 2015-01-02 et 2015-01-04 mais pas au 01-03)
            data.index = data["Invoice_Date"]
            date_range = pd.date_range(year_str+"-01-01", year_str+"-12-31")
            data = data.reindex(date_range, fill_value=0)

            # reindex casse la colonne Invoice_Date, réparer les colonnes date
            data["date"] = data.index.strftime("%m-%d")
            data["weekday"] = data.index.day_name()
            data = data[["Quantity_Invoiced", "weekday", "date"]]

            # Enlever le jour bisextile
            data = data[~((data.index.month == 2) & (data.index.day == 29))]

            data.to_csv(output_path, index=False, sep='\t')

        else : 
            data = pd.read_csv(output_path, sep='\t')

        combined[year_str] = pd.Series(data['Quantity_Invoiced'].to_numpy()) # Series pour ne pas considérer son id lors d'ajout de colonne
        combined[year_str+"_%"] = convert_percentage(combined[year_str])

    combined['mean'] = combined.mean(axis=1)
    combined["mean_%"] = convert_percentage(combined['mean'])
    combined.to_csv(combined_data_path, index=False, sep='\t')
    return combined

def convert_percentage(arr_data):
    percent_dataset = []

    max_val = np.amax(arr_data)
    min_val = np.amin(arr_data)
    delta = max_val - min_val

    for data in arr_data:
        percent_dataset.append((data - min_val) / delta)

    return percent_dataset

if __name__ == "__main__":
    get_primary_data()