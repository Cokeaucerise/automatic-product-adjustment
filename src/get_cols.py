#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np
import pandas as pd

def get_cols(df:pd.DataFrame, columns:list=[])->np.ndarray:
	"""
	Extract the cols with a matching name from the DataFrame

	Args:
		df (pd.DataFrame): Source DataFrame
		columns (list, optional): Cols name to keep. Defaults to [].

	Returns:
		np.ndarray: Extracted Cols
	"""
	
	results = np.zeros((df.shape[0], len(columns)))

	for i in range(len(columns)):
		col = columns[i]
		if col in df.columns:
			results[:,i] = df[col].to_numpy()
		pass

	return results
