#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np
import pandas as pd
import argparse as ap
from pandas import DataFrame
from datetime import datetime, timedelta
from scipy.stats import trim_mean, kurtosis
from scipy.stats.mstats import mode, gmean, hmean
from sklearn.preprocessing import normalize

def get_rows(array:np.ndarray, identifiers:list=[])->np.ndarray:
	"""
	Get the matching rows

	Args:
		array (np.ndarray): dataset
		identifiers (list, optional): value of the indexes to keep. Defaults to [].

	Returns:
		np.ndarray: Extracted rows
	"""
	if array is not None and len(array) > 0:
		return array[[x_id in identifiers for x_id in array[:,0]]]
	return None
