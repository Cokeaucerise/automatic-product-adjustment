import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def plot_2D(df, x, y):
    """créer un graphique 2D
        Parameters
        ----------
        df : pandas dataframe
            les donnees en matrice 2D
        x : int
            column name for horizontal axis value (like date)
        y : int
            column name for vertical axis value (like sales count)
    """
    data = df[y]
    data.index = df[x]
    data.plot(subplots=True)
    plt.show()


def create_time_steps(length):
  return list(range(-length, 0))

def multi_step_plot(history, true_future, prediction, step):
  """dessiner un graphique à partir des données historiques, 
    les prédictions et les vraies valeurs futures. 
    'bo' et 'ro' : premier lettre est sa couleur et deuxième sa forme.
    cest-a-dire premier cercle bleu et le deuxieme rouge cercle.
    
      Parameters
      ----------
      history : numpy array
          les donnees en 
      true_future : numpy array
          matrice 1D ou la colonne ayant les vraies valeurs
      prediction : numpy array
          matrice 1D ou la colonne ayant les valeurs de predictions
      step : int
          la valeurs des pas entre les points
      
  """
  plt.figure(figsize=(12, 6))
  num_in = create_time_steps(len(history))
  num_out = len(true_future)

  plt.plot(num_in, np.array(history[:, 1]), label='History')
  plt.plot(np.arange(num_out)/step, np.array(true_future), 'bo',
           label='True Future')
  if prediction.any():
    plt.plot(np.arange(num_out)/step, np.array(prediction), 'ro',
             label='Predicted Future')
  plt.legend(loc='upper left')
  plt.show()


# Splitter les données en colonnes par année pour comparer la courbe sur un graph
def split_columns_by_year(data, data_column_name, start_year=2015, end_year=2018):
    
    days = pd.date_range("2019-01-01", "2019-12-31")
    splitted = pd.DataFrame({"date":days.strftime("%m-%d")})
    for year in range(start_year, end_year +1):

        year_str = str(year)

        split_year = data[data['date'] >= year_str+'-01-01']
        split_year = split_year[split_year['date'] <= year_str+'-12-31']
        split_year = split_year[split_year['date'] != year_str+'-02-29'] # enlever le jour bisextile

        # df[year_str] = pd.Series(split_year['tavg'].to_numpy()) # Series pour ne pas considérer son id lors d'ajout de colonne
        splitted[year_str] = pd.Series(split_year[data_column_name].to_numpy()) # Series pour ne pas considérer son id lors d'ajout de colonne
    
    # ajouter une colonne de moyenne
    splitted['mean'] = splitted.mean(axis=1)

    return splitted

def plot_data_per_year_compare(data, start_year, end_year, title="daily data per year comparison"):

    # data s'attend à un df de format ci-dessous : 
    #       2013     2014    2015
    # 0   1957.0   2313.0  2122.0
    # 1   3828.0   5720.0  3426.0
    # 2   3854.0   5878.0  3239.0
    # ...

    BASE_COLORS = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    base_color_index = 0
    epochs = range(365)
    plt.figure()

    for year in range(start_year, end_year +1):
        year_str = str(year)
        plt.plot(epochs, data[year_str], BASE_COLORS[base_color_index], label=year_str)
        base_color_index += 1

    plt.title(title)
    plt.legend()

    plt.show()

def plot_daily_single_column(data, column_name, title="daily value"):
    epochs = range(len(data.index))
    plt.plot(data[column_name])
    plt.title(title)
    plt.show()

def plot_multiple(data, column_names, title="daily value"):

    BASE_COLORS = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    base_color_index = 0
    epochs = range(len(data.index))

    for col_name in column_names:
        plt.plot(epochs, data[col_name], BASE_COLORS[base_color_index], label=col_name)
        base_color_index += 1

    plt.legend()
    plt.title(title)
    plt.show()