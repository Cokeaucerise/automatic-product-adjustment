#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import numpy as np
import pandas as pd
import argparse as ap
import matplotlib.pyplot as plt

def plot_mean_predictions_labels(preds:pd.DataFrame, labels:pd.DataFrame):

	dates = preds.columns[1:]

	ids = preds['item_id'].to_numpy()
	labels 	= labels[[i_id in ids for i_id in labels['item_id']]]

	ids = labels['item_id'].to_numpy()
	preds 	= preds[[i_id in ids for i_id in preds['item_id']]]

	ids = preds['item_id'].to_numpy()

	labels = labels[dates]
	labels_values 	= labels.to_numpy()

	preds 		= preds[dates]
	pred_values = preds.to_numpy()

	for i in range(len(dates)):
		x = range(pred_values.shape[0])
		labels_values_slice = labels_values[:,i]
		preds_values_slice 	= pred_values[:,i]

		labels_mean 	= np.mean(labels_values_slice)
		labels_median 	= np.median(labels_values_slice)

		plt.plot(ids, labels_values_slice, 				label='True test values',			color=[.25,.25,.25])
		plt.plot(ids, [labels_mean 		for i in x], 	label='Mean of the test values',	color=[0,.75,0])
		plt.plot(ids, [labels_median 	for i in x], 	label='Median of the test values',	color=[0,0,.75])
		plt.plot(ids, preds_values_slice, 				label='Prediction values',			color=[.75,0,0])
		plt.legend()
		plt.show()
		pass
	pass

if __name__ == "__main__":
	parser = ap.ArgumentParser(description='')
	parser.add_argument('-p', '--predictions', 			type=str, 	help='Prediction file', 											default=None, 		required=True)
	parser.add_argument('-l', '--labels', 				type=str, 	help='labels file', 												default=None, 		required=True)
	parser.add_argument('-d', '--delimiter', 			type=str, 	help='Delimiter character', 										default=',', 		required=False)
	args = parser.parse_args()

	pred 	= pd.read_csv(args.predictions, sep=args.delimiter)
	labels 	= pd.read_csv(args.labels, sep=args.delimiter)

	plot_mean_predictions_labels(pred, labels)