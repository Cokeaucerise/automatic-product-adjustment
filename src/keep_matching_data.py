#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np

def keep_matching_data(x:np.ndarray, y:np.ndarray)->(np.ndarray, np.ndarray):
	"""
	Removed rows from each dataset that doesn't have a matching product

	Args:
		x (np.ndarray, y): features
		np ([type]): labels

	Returns:
		(np.ndarray, np.ndarray): Returns the cleaned datasets as (x, y)
	"""
	# Only keep features with matching label
	x_ids = x[:,0]
	y_ids = y[:,0]
	
	x = x[[x_id in y_ids for x_id in x_ids]]
	y = y[x[:,0].argsort()]

	x = x[x[:,0].argsort()]
	y = y[y[:,0].argsort()]

	return x, y