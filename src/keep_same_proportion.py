#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np
import pandas as pd
from sklearn.preprocessing import KBinsDiscretizer

def keep_same_proportion(labels:np.ndarray, n_bins:int=10)->np.ndarray:
	"""
	Removes the rows that doesn't contains any enthropy & that are null (only 0)

	Args:
		labels (np.ndarray): Labels dataset

	Returns:
		np.ndarray: Cleaned labels
	"""

	labels_ids = labels[:,0].reshape((labels.shape[0], 1))
	labels_values = labels[:,1].reshape((labels.shape[0], labels.shape[1] - 1))

	discretizer = KBinsDiscretizer(n_bins=n_bins, encode='ordinal', strategy='uniform').fit(labels_values)

	discretized = discretizer.transform(labels_values)
	bins_values = np.hstack((labels_ids, discretized))
	values = None

	df = pd.DataFrame(bins_values, columns=['id', 'gp'])
	group_count = df.groupby('gp').count()
	group_median = int(np.median(group_count.to_numpy()))

	for i in range(discretizer.n_bins):
		new_values = (labels[[v == i for v in discretized[:,0]]])[:group_median,:]

		if values is None:
			values = new_values
		else:
			values = np.vstack((values, new_values))
		pass

	return values