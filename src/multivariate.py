#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import tqdm
import numpy as np
from src.get_rows import get_rows
from src.get_cols import get_cols
from src.combine_features import combine_features
from src.keep_matching_data import keep_matching_data
from src.remove_empty_labels import remove_empty_labels
from src.keep_same_proportion import keep_same_proportion

def multivariate_data_combination(p_features:np.ndarray, c_features:np.ndarray, dates_list:list, steps:list, history_size:int, future_size:int, labels:np.ndarray=None)->(np.ndarray, np.ndarray):
	"""
	Generate a multivariate assembled dataset for a LSTM RNN

	Args:
		p_features (np.ndarray): Product features
		c_features (np.ndarray): Context features
		labels (np.ndarray): Labels
		dates_list (list): All the dates that span the training time frame
		steps (list): Range of the steps index
		history_size (int): Size of the history to keep for every features set
		future_size (int): Number of labels to use for future predictions

	Returns:
		(x, y): Combined features (batch, timestep, features) & Labels (batch, labels) structured as multivariate
	"""

	pbar = tqdm.tqdm(total=len(steps))
	task_cb = lambda: pbar.update(1)

	sys.stderr.write(f'\n=== Combining multivariate features & labels ===\n')

	results = [__multivariate_step_data_combination(p_features, c_features, dates_list[i - history_size:i], dates_list[i:i + future_size], labels=labels, parallel=False, callback=task_cb) for i in steps]
	results = [r for r in results if r is not None] # ignore empty matrix

	x 	= np.vstack([r[0] for r in results if r[0] is not None])
	y 	= [r[1] for r in results if r[1] is not None]

	ids = []
	if labels is not None:
		ids = p_features[[i in labels['item_id'].to_numpy() for i in p_features[:,0]]][:,0]
	else:
		ids = np.array(p_features[:,0]).reshape((p_features.shape[0], 1))

	prediction_dates = np.array([dates_list[i:i + future_size] for i in steps])

	if len(y) > 0:
		y = np.vstack(y)
	else:
		y = None

	return x, y, ids, prediction_dates


def __multivariate_step_data_combination(p_features:np.ndarray, c_features:np.ndarray, features_dates:list, labels_dates:list, parallel:bool=False, callback=None, labels:np.ndarray=None):
	"""
	combination and shaping of dataset for a single step.

	Args:
		p_features (np.ndarray)__combine_product_contextfeatures (np.ndarray): Context features
		labels (np.ndarray): Labels
		features_dates (list): Dates range for the features (history)
		labels_dates (list): Dates range for the labels (future)
		parallel (bool, optional): Execute in parallel async. Defaults to False.
		callback (lambda, optional): Callback at the end of execution. Defaults to None.

	Returns:
		(np.ndarray, np.ndarray): (x, y)
	"""
	step_p_features = p_features
	step_labels = labels
	if labels is not None:
		step_labels = get_cols(labels, ['item_id', *labels_dates])
		step_labels = remove_empty_labels(step_labels, threshold=0.1, keep=0)
		# if len(step_labels) > 0:
		# 	step_labels = keep_same_proportion(step_labels, 10)

		step_p_features, step_labels = keep_matching_data(p_features, step_labels)
		step_labels = step_labels[:,1:]
		pass

	step_c_features = get_rows(c_features, features_dates)
	step_features 	= combine_features(step_p_features, step_c_features, labels=labels, include_date_features=False, parallel=parallel)

	callback()
	if step_features.shape[0] > 0:
		return step_features[:,:,1:], step_labels

	pass
