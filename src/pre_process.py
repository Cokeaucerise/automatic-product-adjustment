#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import argparse as ap
import sys
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

def pre_process(features:np.ndarray, n_components:int=0, variance:int=95, mode:str='N')->np.ndarray:
	"""
	Processes the products features using PCA to normalize the dataset

	Args:
		features (np.ndarray): Original products features
		n_components (int, optional): Number of features to preserve. Defaults to 0.
		variance (int, optional): Only keep features with inner variance above or equal to. Defaults to 95.
		mode (str, optional): Chose witch preprocessing mode to use. ['N', 'V']. Defaults to 'N'.

	Returns:
		np.ndarray: Processed features
	"""

	ids = features[:,0]
	ids = ids.reshape(ids.shape[0],1)

	sys.stderr.write(f'\n=== {__name__}: Normalizing data ===\n')

	a_result = StandardScaler().fit_transform(features[:, 1:])
	sys.stderr.write(f'\n=== {__name__}: Executing PCA ===\n')

	if mode == "N":
		a_result = __reduce_pca_by_n_component(a_result, n_components)
	else:
		a_result = __reduce_pca_by_variance(a_result, variance)

	results = np.hstack((ids, a_result))
	return results


def __reduce_pca_by_n_component(data_set:np.ndarray, n_component:int):
	# Set the max to binding dimension
	n_components_max = min(data_set.shape[0], data_set.shape[1])
	# Use arg if defined, else use max
	n_components = n_component if n_component > 0 else n_components_max
	# Reduce to max if above it
	n_components = min(n_components, n_components_max)

	pca = PCA(n_components=n_components)
	return pca.fit_transform(data_set)


def __reduce_pca_by_variance(data_set:np.ndarray, variance:int):
	return PCA(variance / 100).fit_transform(data_set)


if __name__ == "__main__":
	parser = ap.ArgumentParser(description='')
	parser.add_argument('-f', '--delimiter', type=str, help='Delimiter Character', default=',', required=False)
	parser.add_argument('-n', '--components', type=int, help='Nombre de compnent que l on veut avoir',default=2, required=False)
	parser.add_argument('-v', '--variance', type=str, help='Pourcentage de variance', default=95, required=False)
	parser.add_argument('-m', '--mode', type=str, help='Mode de reduction de features', default='V', required=False)
	args = parser.parse_args()

	features = pd.read_csv(sys.stdin, sep=args.delimiter).to_numpy()
	res = pre_process(features)
	np.savetxt(sys.stdout, res, delimiter=args.delimiter)
	pass
