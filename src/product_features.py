#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import re
import os
import sys
import tqdm
import nltk
import math
import statistics
import numpy as np
import pandas as pd
import argparse as ap
import multiprocessing as mp
from os import path
from io import StringIO
from nltk.stem import *
from nltk import word_tokenize
from operator import itemgetter
from collections import Counter
from pandas import DataFrame as df
from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder
from nltk.tag.stanford import StanfordPOSTagger

# nltk.download('punkt')
# nltk.download('universal_tagset')
# nltk.download('averaged_perceptron_tagger')

root_path = os.path.join(os.getcwd(), "src", "stanford_tagger")
tagger_model_path = os.path.join(root_path, "models", "french-ud.tagger")
tagger_jar_path = os.path.join(root_path, "stanford-postagger.jar")

POS_TAGGER:StanfordPOSTagger = StanfordPOSTagger(tagger_model_path, tagger_jar_path ,encoding='utf8')

ITEM_COLS = ['Item_Id', 'Item_Code', 'Description_FRC', 'Accounting_Regrouping_Category', 'Primary_UOM_Code', 'Unit_Weight', 'Weight_UOM_Code', 'Unit_Volume', 'Volume_UOM_Code']
CATS_COLS = ['category_id', 'category_name']

def product_features(items:df, delimiter:str=',', common_words_number:int=100, adj_words_number:int=100)->df:

	items_name = items['Description_FRC'].to_numpy()

	# Extract data from loaded data
	adj_words			= None
	common_words 		= None

	sys.stderr.write(f'\n=== {__name__}: Creating encoders ===\n')
	
	cat_encoder = None
	cats = items.Accounting_Regrouping_Category.unique()
	if len(cats) > 0:
		cats 		= np.reshape(cats, (cats.shape[0], 1))
		cat_encoder = OrdinalEncoder().fit(cats)
	
	w_encoder = None
	weight_uom = items.Weight_UOM_Code.unique()
	if len(weight_uom) > 0:
		weight_uom 	= np.reshape(weight_uom, (weight_uom.shape[0], 1))
		w_encoder 	= OneHotEncoder(handle_unknown='ignore').fit(weight_uom)

	v_encoder = None
	volume_uom = items.Volume_UOM_Code.unique()
	if len(volume_uom) > 0:
		volume_uom 	= np.reshape(volume_uom, (volume_uom.shape[0], 1))
		v_encoder 	= OneHotEncoder(handle_unknown='ignore').fit(volume_uom)

	sys.stderr.write(f'\n=== {__name__}: Tokenizing name ===\n')
	if items_name is not None: common_words = get_common_words(items_name, common_words_number)
	if items_name is not None:
		adj_words	= get_adjs(items_name, adj_words_number)

	features = get_products_primitives(items.to_numpy(), common_words, adj_words, cat_encoder=cat_encoder, w_encoder=w_encoder, v_encoder=v_encoder)

	# Result
	columns = ['id']
	columns += ['weight']
	columns += w_encoder.get_feature_names(['w_uom']).tolist()
	columns += ['volume']
	columns += v_encoder.get_feature_names(['v_uom']).tolist()
	columns += ['cat'] #cat_encoder.get_feature_names(['cat']).tolist()
	columns += [f'common_word_{w}' for w in common_words]
	columns += [f'adj_{w}' for w in adj_words]
	# [id, weight, *w_uom, volume, *v_uom, *cats, *product_common_words, *product_adjs]

	features = df(features, columns=columns)

	return features


def get_products_primitives(items:np.ndarray, common_words:list, adj_words:list, cat_encoder:OneHotEncoder, w_encoder:OneHotEncoder, v_encoder:OneHotEncoder):
	sys.stderr.write(f'\n=== {__name__}: Generating features ===\n')

	pool = mp.Pool(mp.cpu_count())
	pbar = tqdm.tqdm(total=items.shape[0])
	tasks = [
		pool.apply_async(get_product_primitives,
		args=(id, code, name, cat, uom_code, weight, weight_code, volume, volume_code, common_words, adj_words, cat_encoder, w_encoder, v_encoder),
		callback=lambda _: pbar.update(1))
		for id, code, name, cat, uom_code, weight, weight_code, volume, volume_code in items
	]
	pool.close()
	pool.join()

	results = np.array([t.get() for t in tasks])
	results = results[results[:,0].argsort()]
	return results


def get_product_primitives(id, code, name, cat, uom_code, weight, weight_code, volume, volume_code, common_words:list, adj_words:list, cat_encoder:OneHotEncoder, w_encoder:OneHotEncoder, v_encoder:OneHotEncoder):

	weight = str.strip(weight).lower().replace(',', '.')
	weight = 0.0 if weight == 'null' or weight == '' else float(weight)

	volume = str.strip(volume).lower().replace(',', '.')
	volume = 0.0 if volume == 'null' or volume == '' else float(volume)

	w_uom = []
	if w_encoder is not None:
		w_uom = w_encoder.transform([[weight_code]]).toarray()[0,:]

	v_uom = []
	if v_encoder is not None:
		v_uom = v_encoder.transform([[volume_code]]).toarray()[0,:]

	cats = []
	if cat_encoder is not None:
		cats = cat_encoder.transform([[cat]])[0]
	
	# We append the X most common words in the product description
	product_common_words = []
	if common_words is not None:
		product_common_words = get_matching_words(description_to_words(name), common_words)

	# We append the adjectives
	product_adjs = []
	if adj_words is not None:
		product_adjs = get_matching_words(description_to_words(name), adj_words)

	return [id, weight, *w_uom, volume, *v_uom, *cats, *product_common_words, *product_adjs]


def get_matching_words(locale_words:list, global_words:list) -> list:
	return [1.0 if w in locale_words else 0.0 for w in global_words]


def description_to_words(description:str) -> list:
	words = description.lower().split(' ')
	words = filter(lambda w: len(w) > 2, words)
	words = map(lambda w: re.sub('\W+', ' ', w).replace(' ', ''), words)
	return list(words)


def get_common_words(descriptions:list, n_count:int = 5) -> list:

	words_dict = {}

	for description in descriptions:
		for word in description_to_words(description):
			if word not in words_dict:
				words_dict[word] = 0
			words_dict[word] += 1
			pass
		pass

	sorted_words_tuple = sorted(words_dict.items(), key=itemgetter(1), reverse=True)
	sorted_words = list(map(lambda w: w[0], sorted_words_tuple))

	empty_fill = ['' for x in range(n_count - len(sorted_words))]

	return [*sorted_words, *empty_fill][:n_count]


def get_adjs(names:list, n_count:int = 500) -> list:
	adjs = []

	BATCH_SIZE = 1000

	iterations = range(0, len(names), BATCH_SIZE)

	pool = mp.Pool(mp.cpu_count())
	pbar = tqdm.tqdm(total=len(names))
	tasks = [pool.apply_async(get_adj, args=(' '.join(names[i:i+BATCH_SIZE-1])), callback=lambda _: pbar.update(BATCH_SIZE)) for i in iterations]
	pool.close()
	pool.join()

	for t in tasks: adjs += t.get()
	# Remove the duplicates
	adjs = list(set(adjs))

	# Sort by most common adjs
	adjs.sort(key=Counter(adjs).get, reverse=True)

	empty_fill = ['' for x in range(n_count - len(adjs))]

	return [*adjs, *empty_fill][0:n_count]


def get_adj(*chars:str) -> list:
	text = ''.join(chars)
	tokenized_adjs = [tag[0] for tag in tokenize_data(text) if 'ADJ' in tag[1] or 'A=' in tag[1]]
	# root_adjs = find_root(tokenized_adjs)
	return tokenized_adjs


def tokenize_data(name:str) -> list:
	name = name.lower()
	# On passe le tagger russe en premier
	words = word_tokenize(name, language="french")
	words_no_punctuation = [word for word in words if word.isalnum()]
	tagged_words = pos_tag(words_no_punctuation)

	# Si plus de la moitié sont inconnus (magic number = 1.8), ça veut dire que le produit est en anglais.
	# On passe le tagger anglais
	count_tags = Counter(elem[1] for elem in tagged_words)
	if 'NONLEX' in count_tags:
		if count_tags.get('NONLEX') > len(words_no_punctuation) / 1.8:
			words = word_tokenize(name)
			words_no_punctuation = [word for word in words if word.isalnum()]
			tagged_words = pos_tag(words_no_punctuation)

	return tagged_words


def pos_tag(tokens):
		return POS_TAGGER.tag(tokens)


def find_root(adjs):
	# Find the root of each word
	stemmed_adj = []
	stemmer = SnowballStemmer("french")
	default_stemmer = SnowballStemmer("english")
	# rus_stemmer = SnowballStemmer("russian")

	for w in adjs:
		stem = stemmer.stem(w)
		if stem == w:
			stemmed_adj.append(default_stemmer.stem(w))
		else:
			stemmed_adj.append(stem)

	return stemmed_adj


if __name__ == "__main__":

	description = """
	Items required cols: ['Item_Id', 'Item_Code', 'Description_FRC', 'Accounting_Regrouping_Category', 'Primary_UOM_Code', 'Unit_Weight', 'Weight_UOM_Code', 'Unit_Volume', 'Volume_UOM_Code'].
	"""

	parser = ap.ArgumentParser(description=description)
	parser.add_argument('-id', '--i-delimiter', 	type=str, help='Input delimiter Character', default=',', required=False)
	parser.add_argument('-od', '--o-delimiter', 	type=str, help='Ouput delimiter Character', default=',', required=False)
	args = parser.parse_args()

	items = pd.read_csv(sys.stdin, sep=args.i_delimiter, usecols=ITEM_COLS, keep_default_na=False)

	items = items[(items.Description_FRC != 'erreur')]

	features = product_features(items)
	features.to_csv(sys.stdout, index=False, sep=args.o_delimiter)
	pass