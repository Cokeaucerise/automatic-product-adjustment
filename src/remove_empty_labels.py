#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import numpy as np

def remove_empty_labels(labels:np.ndarray, threshold:float=0.0, keep:float=0.0, use_mean:bool=False)->np.ndarray:
	"""
	Removes the rows that doesn't contains any enthropy & that are null (only 0)

	Args:
		labels (np.ndarray): Labels dataset

	Returns:
		np.ndarray: Cleaned labels
	"""

	values = labels[[(l.mean() if use_mean else l.max()) >= threshold for l in labels[:,1:]]]

	if keep > 0.0:
		portion_size = int(values.shape[0] * keep)

		empty = labels[[(l.mean() if use_mean else l.max()) < threshold for l in labels[:,1:]]]
		np.random.shuffle(empty)
		
		empty = empty[:portion_size]
		values = np.vstack((values, empty))
		np.random.shuffle(values)
		pass

	return values