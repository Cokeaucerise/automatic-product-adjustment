#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Project :
	Service d'ajustement des priorités de ventes automatisées

Students :
	Charles Paquin 		- PAQC03029507
	Olivier Lamarre 	- LAMO13069505
	Kyoshiro Kaizuka 	- KAIK21089205
	Maxim Diouskine 	- DIOM28049202
"""

import os
import sys
import sklearn
import datetime
import matplotlib
import numpy as np
import pandas as pd
import argparse as ap
import tensorflow as tf
import matplotlib.pyplot as plt
from os import path
from io import StringIO
from sklearn import metrics
from sklearn.preprocessing import LabelEncoder, minmax_scale, KBinsDiscretizer
from sklearn.model_selection import train_test_split, KFold
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout, BatchNormalization, LSTM, Bidirectional
from tensorflow.keras.activations import sigmoid, relu, linear, tanh, softmax

def rnn(name:str, x:np.ndarray, y:np.ndarray=None, epochs:int=5, batch_size:int=100, dropout:float=0.2, k_folds:int=5, delimiter:str=',', models_folder:str= './nn_models'):
	"""
	Make a prediction using the model found in the folder with the given model name.
	If labels are given and no model can be found, a new model will be trained using the features (x) and labels (y).
	If labels are given and a model already exists, the existing model will be boosted using the features (x) and labels (y).

	Args:
		name (str): Name of the model to use or create.
		x (np.ndarray): Features
		y (np.ndarray, optional): Labels. Defaults to None.
		epochs (int, optional): Number of epoch to use when training. Defaults to 5.
		batch_size (int, optional): batch size to use when training. Defaults to 100.
		dropout (float, optional): Dropout rate to use when training (Currently unused). Defaults to 0.25.
		num_classes (int, optional): Number of classes for the metrics to use when training. Defaults to 20.
		delimiter (str, optional): Delimiter string to use when serializing results. Defaults to ','.
		models_folder (str, optional): Root folder to use to locate models. Defaults to './nn_models'.
	"""

	model_folder = os.path.join(models_folder, name)

	if y is None:
		print(f"\n//=== {__name__}: PREDICTING USING MODEL {name} ===//\n")
		predictions = __make_prediction(model_folder, x)
		return predictions
	else:

		batch, history_size, features_size = x.shape
		future_size = y.shape[1]

		model = __get_model_or_defautl(name, model_folder, (history_size, features_size), future_size, dropout)
		model = __train_nn(model_folder, model, x, y, epochs, batch_size, k_folds=k_folds)

		__save_model(model_folder, model)
		pass
	pass

# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
#  ____  _   _ _   _
# |  _ \| \ | | \ | |
# | |_) |  \| |  \| |
# |  _ <| |\  | |\  |
# |_| \_\_| \_|_| \_|
# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def __make_prediction(f_path:str, x:np.ndarray)->np.ndarray:
	"""
	Make a prediction with the model located in the f_path and the x features

	Args:
		f_path (str): Path to the model folder
		x (np.ndarray): Features matrix

	Returns:
		ndarray: Labels
	"""
	# Make prediction by loading model from name
	model = __compile_model(__load_model(f_path), 1)

	return model.predict(x)


def __get_model_or_defautl(name:str, model_folder:str, input_shape:tuple, future_size:int, dropout:int)->tf.keras.Model:
	"""
	Load the RNN Model located at the folder path if any exists, else create a new one at the specified path.

	Args:
		name (str): Model name
		model_folder (str): Model folder path
		input_shape (tuple): Input shape of the model
		future_size (int): Size of the future prediction to take in account
		dropout (int): Dropout rate of the model (unused)

	Returns:
		tf.keras.Model: Keras Model
	"""

	if __model_exist(model_folder):
		print(f"\n//=== {__name__}: BOOSTING EXISTING MODEL {name} ===//\n")
		# Boost existing model and save model with name
		return __compile_model(__load_model(model_folder), future_size)
	else:
		print(f"\n//=== {__name__}: TRAIN NEW MODEL {name} ===//\n")
		model = generate_base_model(name, input_shape, future_size, dropout)
		return __compile_model(model, future_size)


def __train_nn(f_path:str, model:tf.keras.Model, x:np.ndarray, y:np.ndarray, epochs:int, batch_size:int, k_folds:int)->tf.keras.Model:
	"""
	Train a model with the given features and labels.
	The source features and labels will be split in 3 dataset: Train, validate, test
	- The Training set will be use to train the model.
	- The Validation set is used to check the accuracy of the resulting training of every epochs
	- The Test set is used at the end of the training as a out of context validation data set to check accuracy and generate prediction metrics

	Args:
		f_path (str): Model folder path to save new model, training logs and metrics
		model (tf.keras.Model): Model to train
		x (np.ndarray): Features
		y (np.ndarray): Labels
		label_encoder (LabelEncoder): Label encoder to use for the metrics
		epochs (int): Number of epochs to do when training
		batch_size (int): Batch size of every epoch

	Returns:
		tf.keras.Model: Trained model
	"""

	sys.stderr.write(f'\n=== {__name__}: Multi input shape = {x.shape} {y.shape} ===\n')

	x_trainval, x_test, y_trainval, y_test 	= train_test_split(x, y, test_size=0.1)
	# x_train, 	x_val, 	y_train, 	y_val 	= train_test_split(x_trainval, y_trainval, test_size=0.1)

	timestamp = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-').replace('.', '-')

	k_fold_i = 0
	for train_index,test_index in KFold(k_folds).split(x_trainval):
		k_fold_i += 1
		sys.stderr.write(f'\n=== Training fold: {k_fold_i} ===\n')

		output_dir = os.path.join(f_path, f'logs-{timestamp}/k_fold-{k_fold_i}')

		tb_callback 	= tf.keras.callbacks.TensorBoard(log_dir=output_dir, write_graph=True, write_images=True, update_freq='epoch')
		early_stopping 	= tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=50, restore_best_weights=True)

		x_train, x_val = x_trainval[train_index], x_trainval[test_index]
		y_train, y_val = y_trainval[train_index], y_trainval[test_index]

		model.fit(x_train, y_train, validation_data=(x_val, y_val), epochs=epochs, batch_size=batch_size, callbacks=[tb_callback, early_stopping])

		y_pred = model.predict(x_test)
		__save_graph(output_dir, y_test, y_pred, model)
		__write_metrics(output_dir, y_test, y_pred, model)


	output_dir = os.path.join(f_path, f'logs-{timestamp}')

	y_pred = model.predict(x_test)
	__save_graph(output_dir, y_test, y_pred, model)
	__write_metrics(output_dir, y_test, y_pred, model)

	return model


def generate_base_model(model_name:str, input_shape:tuple, output_shape:int, dropout:float)->tf.keras.Model:
	"""
	Generate a model following the basic structure of a LSTM

	Args:
		model_name (str): Name of the model
		input_shape (tuple): Input shape for the LSTM. (history, features)
		output_shape (int): Ouput size of the LSTM. (future)
		dropout (float): Dropout rate beetween LSTM layers (unused).

	Returns:
		tf.keras.Model: Generic model without weights
	"""
	# cuDNN GPU compatible argument
	# activation == tanh
	# recurrent_activation == sigmoid
	# recurrent_dropout == 0
	# unroll is False
	# use_bias is True

	history_size, feature_size = input_shape

	model = tf.keras.models.Sequential(name=model_name)

	model.add(LSTM(history_size, input_shape=input_shape, return_sequences=True))
	model.add(LSTM(history_size, input_shape=input_shape))
	model.add(Dropout(dropout))
	model.add(Dense(output_shape, activation=sigmoid))

	return model


# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
#  __  __           _      _       _   _ _   _ _
# |  \/  | ___   __| | ___| |     | | | | |_(_) |___
# | |\/| |/ _ \ / _` |/ _ \ |_____| | | | __| | / __|
# | |  | | (_) | (_| |  __/ |_____| |_| | |_| | \__ \
# |_|  |_|\___/ \__,_|\___|_|      \___/ \__|_|_|___/
# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def __save_model(f_path:str, model:tf.keras.Model):
	"""
	Save a Keras NN model with weights

	Args:
		f_path (str): Model folder path
		model (tf.keras.Model): Model to save
	"""

	if not os.path.exists(f_path):
			os.makedirs(f_path)

	model_path = os.path.join(f_path, 'model.json')
	weights_path = os.path.join(f_path, 'weights.h5')

	model_json = model.to_json()
	with open(model_path, 'w') as json_file:
		json_file.write(model_json)

	model.save_weights(weights_path)
	pass


def __load_model(model_path:str)->tf.keras.Model:
	"""
	Load a model from the given path

	Args:
		model_path (str): Model folder path

	Returns:
		tf.keras.Model: Keras model with weights
	"""

	model_json_path = os.path.join(model_path, 'model.json')
	model_weights = os.path.join(model_path, 'weights.h5')

	model_json = open(model_json_path, 'r').read()

	model = tf.keras.models.model_from_json(model_json)
	model.load_weights(model_weights)
	return model


def __model_exist(model_path:str)->bool:
	"""
	Return if a model file & weights exists in the folder

	Args:
		model_path (str): Model folder path

	Returns:
		bool: Exists
	"""
	return path.isfile(path.join(model_path, 'model.json')) and path.isfile(path.join(model_path, 'weights.h5'))


def __compile_model(model:tf.keras.Model, future_size:int)->tf.keras.Model:
	"""
	Compile model with correct parameters for GPU in cuDNN

	Args:
		model (tf.keras.Model): Model to compile

	Returns:
		tf.keras.Model: Compiled model
	"""
	optimizer 	= tf.keras.optimizers.Adam()
	loss 		= tf.keras.losses.MeanAbsoluteError()
	metrics		= ['mae']

	model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
	return model


# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
#  __  __      _        _
# |  \/  | ___| |_ _ __(_) ___ ___
# | |\/| |/ _ \ __| '__| |/ __/ __|
# | |  | |  __/ |_| |  | | (__\__ \
# |_|  |_|\___|\__|_|  |_|\___|___/
# _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def __write_metrics(f_path:str, y_true, y_pred, model:tf.keras.Model):

	if not os.path.exists(f_path): os.makedirs(f_path)

	metrics_path = os.path.join(f_path, 'metrics.txt')
	with open(metrics_path, 'a+') as f:

		kbin_encoder = KBinsDiscretizer(n_bins=20, encode='ordinal', strategy='quantile').fit(y_true, y_pred)

		f.write('\n_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_\n')
		f.write('\n')

		y_true = kbin_encoder.transform(y_true)
		y_pred = kbin_encoder.transform(y_pred)

		writeline = lambda x: f.write(x + '\n')
		model.summary(print_fn=writeline)

		f.write('\nBalanced accuracy score:\n')
		f.writelines(str(metrics.balanced_accuracy_score(y_true, y_pred)) + '\n')

		f.write('\nMean absolute error:\n')
		f.writelines(str(metrics.mean_absolute_error(y_true, y_pred)) + ' / 20 \n')
		
		f.write('\nMedian absolute error:\n')
		f.writelines(str(metrics.median_absolute_error(y_true, y_pred)) + ' / 20 \n')

		f.write('\nConfusion matrix:\n')
		f.writelines(str(metrics.confusion_matrix(y_true, y_pred)) + '\n')

		f.write('\nClassification report:\n')
		f.writelines(str(metrics.classification_report(y_true, y_pred, digits=2, zero_division=0)) + '\n')
		pass

def __save_graph(f_path:str, y_true, y_pred, model:tf.keras.Model):
	"""
	Write the metrics of the predictions made versus the real values

	Args:
		f_path (str): Folder path in witch to write the results
		y_test ([type]): Real labels
		y_pred ([type]): Prediction labels
		model (tf.keras.Model): Model
		label_encoder (LabelEncoder): Label encoder to discretize results
	"""

	if not os.path.exists(f_path): os.makedirs(f_path)

	timestamp = str(datetime.datetime.now().timestamp()).split('.')[0]

	np.set_printoptions(linewidth=np.inf)

	chart_path = os.path.join(f_path, f'chart.png')
	x = range(len(y_true.ravel()))
	chart = plt.figure()

	y_vals = np.vstack((y_true.ravel().transpose(), y_pred.ravel().transpose()))
	y_vals= y_vals[:,y_vals[0,:].argsort()]

	y_true = y_vals[0,:]
	y_pred = y_vals[1,:]

	test_mean = np.mean(y_true)
	test_median = np.median(y_true)

	plt.plot(x, y_true, 						label='True test values',			color=[.25,.25,.25])
	plt.plot(x, y_pred, 						label='Prediction values',			color=[.75,0,0])
	plt.plot(x, [test_mean 		for i in x], 	label='Mean of the test values',	color=[0,.75,0])
	plt.plot(x, [test_median 	for i in x], 	label='Median of the test values',	color=[0,0,.75])
	plt.legend()

	chart.savefig(chart_path)
	pass


def __label_encoder(num_classes:int, step:float=0.05)->LabelEncoder:
	"""
	Create a label encoder that create a label for every percentage slice that one fraction of a class cover (20 classes = 1/20 per label = 0.05)

	Args:
		num_classes (int): Number of classes to use for the label creation

	Returns:
		LabelEncoder: Label encoder to use to transform
	"""
	classes = np.array([i * step for i in range(num_classes + 1)])
	classes = np.round(classes.astype(np.float), decimals=2)
	return LabelEncoder().fit(classes)


def __group_labels(y:np.ndarray, num_classes:int, max_val:int=100, offset_group:bool=False):
	factor = float(max_val / num_classes)
	offset = factor / 2 if offset_group else 0

	for i in range(num_classes):
		val = i * factor
		l, r = (val - offset, val + factor - offset)
		y[(l < y) & (y < r)] = np.float(val)
		pass

	return np.round(y.astype(np.float), decimals=2)